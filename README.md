### FCSpipelineEMBL_KNIME
FCSpipelineEMBL_KNIME is a [**KNIME**](https://www.knime.com/) workflow for the interactive and automated analysis of FCS-calibrated imaging data. FCSpipelineEMBL_KNIME is converting fluorescence intensities into absolute concentrations of fluorescent molecules (absolute numbers of fluorescent molecules in the confocal volume). The workflow input consists of FCS data files processed in [**Fluctuation Analyzer**](http://www.fluctuations.de/) and raw image data. The output of the workflow is calibrated imaging data represented by calibration parameters for converting image intensities into local concentrations and concentration image maps produced from calibration parameters.

Please refer to the [**Wiki**](https://git.embl.de/grp-almf/FCSpipelineEMBL_KNIME/-/wikis/home) for user guidelines and detailed instructions. For the required input data formats see [structure of files](https://git.embl.de/grp-almf/FCSpipelineEMBL_KNIME/-/wikis/structure-of-files). 


### Contents
* FCSpipelineEMBL_KNIME.knwf is the latest stable version of FCSpipelineEMBL_KNIME workflow, that can be imported into KNIME.
* FCSpipelineEMBL_KNIME - the directory with the latest development files of the FCSpipelineEMBL_KNIME.
* Calibration - an example dataset with raw FCS-calibrated imaging data to test the FCSpipelineEMBL_KNIME workflow. The data is published in  Cai Y, et al. (2018) 561: 411-415 (https://doi.org/10.1038/s41586-018-0518-z).
* output - the directory with all output files created by FCSpipelineEMBL_KNIME.
* fcspy.yml - a configuration file to create Python Anaconda environment, needed in the [installation step](https://git.embl.de/grp-almf/FCSpipelineEMBL_KNIME/-/wikis/installation).

### Authors
**Vitali Vistunou** <br>
current affiliation:
vistunov.vk@phystech.edu <br>
Moscow Institute of Physics and Technology

Project supervisors and contributors: [**Aliaksandr Halavatyi**](https://www.embl.de/services/core_facilities/almf/members/) and [**Arina Rybina**](https://www-ellenberg.embl.de/people).

The pipeline was developed in the [**Ellenberg Lab**](https://www-ellenberg.embl.de/) in collaboration with [**Advanced Light Microscopy Facility**](https://www.embl.de/services/core_facilities/almf/) (ALMF), EMBL, Heidelberg, Germany.

### Acknowledgments

We would like to thank Malte Wachsmuth for his general advice on FCS procedures, Samuel Collombet for a thorough testing the workflow and regular feedback on development, and Andreas Brunner for his comments on documentation and features.

We also would like to thank Jan Ellenberg and Rainer Pepperkok for hosting and support, and the members of the Ellenberg lab and ALMF for the collaborative environment.

The development of this workflow was supported by an [**Allen Distinguished Investigator Program Grant**](https://alleninstitute.org/what-we-do/frontiers-group/distinguished-investigators/) to Jan Ellenberg, through The Paul G. Allen Frontiers Group.

### Contacts
People to get in contact with for any questions regarding the FCSpipelineEMBL_KNIME workflow:
* Vitali Vistunou (vistunov.vk@phystech.edu)
* Aliaksandr Halavatyi (aliaksandr.halavatyi@embl.de)
* Arina Rybina (arina.rybina@embl.de)

### Recommendations for Reading and Tutorials

* FCS calibrated imaging - [PROTOCOL](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6609853/)
* Fluctuation Analyzer 4G - [USER GUIDE](http://fluctuations.de/HT-FCS/FA/FluctuationAnalyzerUserGuide_v1.pdf), [Reference ](https://www.nature.com/articles/nbt.3146) 
* Introduction to Image Processing in KNIME - short Intro [WEBINAR](https://www.youtube.com/watch?v=ofpSlAEkHNs)


### Citation

For citation, please refer to this GitLab page: https://git.embl.de/grp-almf/FCSpipelineEMBL_KNIME
